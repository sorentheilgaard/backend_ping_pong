﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PingPongBackend.Permissions
{
    public enum PermissionEnum
    {
        CanReadMatch,
        CanCreateMatch,
        CanEditMatch,
        CanDeleteMatch
    }
}
