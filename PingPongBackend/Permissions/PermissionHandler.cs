﻿using Microsoft.AspNetCore.Authorization;
using PingPongBackend.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PingPongBackend.Permissions
{
    public class PermissionHandler : AuthorizationHandler<PermissionRequirement>
    {
        private readonly PingPongDbContext _context;

        public PermissionHandler(PingPongDbContext context)
        {
            _context = context;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (context.User == null)
            {
                return Task.CompletedTask;
            }

            var hasPermission = CheckUserPermissions(context.User, requirement._permission);

            if (hasPermission) 
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;

        }

        private bool CheckUserPermissions(ClaimsPrincipal user, PermissionEnum permission)
        {
            var res = user.Claims.Where(u => u.Type.ToString() == permission.ToString());
            return res.Count() > 0;
        }
    }
}
