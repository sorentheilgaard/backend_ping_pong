﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PingPongBackend.Models
{
    public class Match
    {
        public int Id { get; set; }
        public int PlayerOneScore { get; set; }
        public int PlayerTwoScore { get; set; }
        public int AmountOfGames { get; set; }

        public ICollection<Player> Players { get; set; }
        public ICollection<Game> Games { get; set; }
    }
}
