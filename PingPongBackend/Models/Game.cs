﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PingPongBackend.Models
{
    public class Game
    {
        public int Id { get; set; }
        public int PlayerOneScore { get; set; }
        public int PlayerTwoScore { get; set; }
        public int MatchId { get; set; }
        public Match Match { get; set; }
        public int BestOfPoints { get; set; }
    }
}