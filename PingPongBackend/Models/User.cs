﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PingPongBackend.Models
{
    public class User : IdentityUser
    {
        public ICollection<Permission> permissions { get; set; }
    }
}