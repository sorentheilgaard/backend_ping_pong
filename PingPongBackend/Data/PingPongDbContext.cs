﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PingPongBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PingPongBackend.Data
{
    public class PingPongDbContext : IdentityDbContext<User>
    {
        public PingPongDbContext(DbContextOptions<PingPongDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Permission>().HasIndex(p => p.Name).IsUnique();
            modelBuilder.Entity<Permission>().Property(p => p.Name).HasMaxLength(255);
        }

        public DbSet<User> users { get; set; }
        public DbSet<Game> games { get; set; }
        public DbSet<Match> matches { get; set; }
        public DbSet<Player> players { get; set; }
        public DbSet<Permission> permissions { get; set; }

    }
}
