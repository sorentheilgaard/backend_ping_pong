﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PingPongBackend.Data;
using PingPongBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PingPongBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchesController : Controller
    {
        private readonly PingPongDbContext _context;

        public MatchesController(PingPongDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Match>>> GetMatches()
        {
            var matches = await _context.matches.Include(i => i.Games).Include(i => i.Players)
                .ToListAsync();
            return Ok(matches);
        } 

        [HttpGet("{id}")]
        public async Task<ActionResult<Match>> GetMatchById(int id)
        {
            var match = await _context.matches.FindAsync(id);

            if (match == null)
            {
                return NotFound();
            }

            return Ok(match);
        }

        [HttpPost]
        public async Task<ActionResult<Match>> CreateMatch([FromBody] Match match)
        {
            await _context.AddAsync(match);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetMatches), new { id = match.Id }, match);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Match>> UpdateMatch([FromRoute] int id, [FromBody] Match match)
        {
            if (id != match.Id)
            {
                return BadRequest();
            }

            var entity = await _context.matches.FindAsync(id);

            if(entity == null)
            {
                return NotFound();
            }

            entity = match;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Match>> DeleteMatch([FromRoute] int id)
        {
            var match = await _context.matches.FindAsync(id);

            if (match == null)
            {
                return NotFound();
            }

            _context.matches.Remove(match);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }
}
